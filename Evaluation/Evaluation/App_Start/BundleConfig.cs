﻿using System.Web;
using System.Web.Optimization;

namespace Evaluation
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                         "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                       "~/Scripts/jquery.unobtrusive*",
                       "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                   "~/Scripts/kendo/kendo.all.min.js",
                   "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                   "~/Scripts/kendo/jszip.min.js",
                   "~/Scripts/kendo/cultures/kendo.culture.es-MX.min.js",
                   "~/Scripts/kendo/messages/kendo.messages.es-ES.min.js"));
            

            //*************STYLESHEETS*************
            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                    "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/Content/kendo").Include(
                    "~/Content/kendo/kendo.common.min.css",
                    "~/Content/kendo/kendo.bootstrap.min.css",
                    "~/Content/kendo/kendo.common-bootstrap.min.css"));

        }
    }
}
