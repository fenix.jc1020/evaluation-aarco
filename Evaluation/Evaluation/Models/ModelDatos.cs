﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Evaluation.Models
{
    public class ModelDatos
    {
        public bool Error { get; set; }
        public string Mensaje { get; set; }

        public string Marca { get; set; }
        public string Submarca { get; set; }
        public string Modelo { get; set; }
        public string Descripcion { get; set; }
        public string CodigoPostal { get; set; }
        public string Estado { get; set; }
        public string Municipio { get; set; }
        public string Colonia { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Genero { get; set; }
    }
}