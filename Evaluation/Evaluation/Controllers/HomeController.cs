﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RestSharp;
using Evaluation.Models;
using Newtonsoft.Json;
using Evaluation.Dto;

namespace Evaluation.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index(ModelDatos model)
        {
            model.FechaNacimiento = DateTime.Now;
            var client = new RestClient("https://apitestcotizamatico.azurewebsites.net/api/catalogos");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "c9f14841-c01d-4d00-b9e7-e6866e348b02");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("undefined", "{\n\t\"NombreCatalogo\":  \"Marca\",\n\t\"Filtro\": \"1\",\n\t\"IdAplication\": 2\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            if (response.IsSuccessful)
            {
                MarcaDto marcaDto = new MarcaDto();
                marcaDto.catalogo = new List<CatalogoMarcaDto>();
                dynamic obj = JsonConvert.DeserializeObject<dynamic>(response.Content);
                marcaDto.catalogo = JsonConvert.DeserializeObject<List<CatalogoMarcaDto>>(obj["CatalogoJsonString"].ToString());
                ViewBag.Marcas = marcaDto.catalogo.CreateSelectList("iIdMarca", "sMarca", true);

                List<CatalogoSubmarcaDto>  submarca = new List<CatalogoSubmarcaDto>();
                ViewBag.Submarcas = submarca.CreateSelectList("iIdSubmarca", "sSubmarca", true);

                List<CatalogoModeloDto> modelo = new List<CatalogoModeloDto>();
                ViewBag.Modelos = modelo.CreateSelectList("iIdModelo", "sModelo", true);

                List<CatalogoDescripcionDto> desc = new List<CatalogoDescripcionDto>();
                ViewBag.Descripciones = desc.CreateSelectList("iIdDescripcionModelo", "sDescripcion", true);
            }
            var generos = new List<SelectListItem>();
            generos.Add(new SelectListItem() { Text = "-- Seleccione --", Value = string.Empty });
            generos.Add(new SelectListItem() { Text = "Masculino", Value = "M" });
            generos.Add(new SelectListItem() { Text = "Femenino", Value = "F" });
            ViewBag.Generos = generos;
            return View(model);
        }
        [HttpPost]
        public JsonResult SetSubmarca(string marca)
        {
            var client = new RestClient("https://apitestcotizamatico.azurewebsites.net/api/catalogos");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "c9f14841-c01d-4d00-b9e7-e6866e348b02");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("undefined", "{\n\t\"NombreCatalogo\":  \"Submarca\",\n\t\"Filtro\": \""+ marca + "\",\n\t\"IdAplication\": 2\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            if (response.IsSuccessful)
            {
                SubmarcaDto submarcaDto = new SubmarcaDto();
                submarcaDto.catalogo = new List<CatalogoSubmarcaDto>();
                dynamic obj = JsonConvert.DeserializeObject<dynamic>(response.Content);
                submarcaDto.catalogo = JsonConvert.DeserializeObject<List<CatalogoSubmarcaDto>>(obj["CatalogoJsonString"].ToString());
                ViewBag.Submarcas = submarcaDto.catalogo.CreateSelectList("iIdSubmarca", "sSubmarca", true);
            }
            return Json(new SelectList(ViewBag.Submarcas, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetModelo(string submarca)
        {
            var client = new RestClient("https://apitestcotizamatico.azurewebsites.net/api/catalogos");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "c9f14841-c01d-4d00-b9e7-e6866e348b02");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("undefined", "{\n\t\"NombreCatalogo\":  \"Modelo\",\n\t\"Filtro\": \"" + submarca + "\",\n\t\"IdAplication\": 2\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            if (response.IsSuccessful)
            {
                ModeloDto modeloDto = new ModeloDto();
                modeloDto.catalogo = new List<CatalogoModeloDto>();
                dynamic obj = JsonConvert.DeserializeObject<dynamic>(response.Content);
                modeloDto.catalogo = JsonConvert.DeserializeObject<List<CatalogoModeloDto>>(obj["CatalogoJsonString"].ToString());
                ViewBag.Modelos = modeloDto.catalogo.CreateSelectList("iIdModelo", "sModelo", true);
            }
            return Json(new SelectList(ViewBag.Modelos, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDescripcion(string modelo)
        {
            var client = new RestClient("https://apitestcotizamatico.azurewebsites.net/api/catalogos");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "c9f14841-c01d-4d00-b9e7-e6866e348b02");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("undefined", "{\n\t\"NombreCatalogo\":  \"DescripcionModelo\",\n\t\"Filtro\": \"" + modelo + "\",\n\t\"IdAplication\": 2\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            if (response.IsSuccessful)
            {
                DescripcionDto descripcionDto = new DescripcionDto();
                descripcionDto.catalogo = new List<CatalogoDescripcionDto>();
                dynamic obj = JsonConvert.DeserializeObject<dynamic>(response.Content);
                descripcionDto.catalogo = JsonConvert.DeserializeObject<List<CatalogoDescripcionDto>>(obj["CatalogoJsonString"].ToString());
                ViewBag.Descripciones = descripcionDto.catalogo.CreateSelectList("iIdDescripcionModelo", "sDescripcion", true);
            }
            return Json(new SelectList(ViewBag.Descripciones, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDomicilio(string cp)
        {
            DomicilioDto domicilioDto = new DomicilioDto();
            var client = new RestClient("https://api.copomex.com/query/info_cp/" + cp + "?type=simplified&token=75f6a33b-25ba-4ca4-85ef-8b45af6cdcb4");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "47e158d2-732c-4360-b628-67bf20e7f37c");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("undefined", "undefined=", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            if (response.IsSuccessful)
            {
                dynamic obj = JsonConvert.DeserializeObject<dynamic>(response.Content);
                domicilioDto = JsonConvert.DeserializeObject<DomicilioDto>(obj["response"].ToString());
            }
            return Json(domicilioDto, JsonRequestBehavior.AllowGet);
        }
        
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SaveDataValues(ModelDatos model)
        {
            ResponseDto responseDto = new ResponseDto();
            if(model.Marca == null || model.Marca == "")
            {
                responseDto.Error = true;
                responseDto.Mensaje += "\n Debe indicar una Marca";
            }
            if (model.Submarca == null || model.Submarca == "")
            {
                responseDto.Error = true;
                responseDto.Mensaje += "\n Debe indicar una Submarca";
            }
            if (model.Modelo == null || model.Modelo == "")
            {
                responseDto.Error = true;
                responseDto.Mensaje += "\n Debe indicar un Modelo";
            }
            if (model.Descripcion == null || model.Descripcion == "")
            {
                responseDto.Error = true;
                responseDto.Mensaje += "\n Debe indicar una Descripción";
            }
            if (model.CodigoPostal == null || model.CodigoPostal == "" || model.CodigoPostal.Length != 5)
            {
                responseDto.Error = true;
                responseDto.Mensaje += "\n Debe indicar un Código Postal válido";
            }

            if (model.Estado == null || model.Estado == "")
            {
                responseDto.Error = true;
                responseDto.Mensaje += "\n Debe indicar un Estado";
            }
            if (model.Municipio == null || model.Municipio == "")
            {
                responseDto.Error = true;
                responseDto.Mensaje += "\n Debe indicar un Municipio";
            }
            if (model.Colonia == null || model.Colonia == "")
            {
                responseDto.Error = true;
                responseDto.Mensaje += "\n Debe indicar una Colonia";
            }
            if (GetDifferenceInYears(model.FechaNacimiento, DateTime.Now) < 18)
            {
                responseDto.Error = true;
                responseDto.Mensaje += "\n La edad no puede ser menor a 18 años";
            }
            if (model.Genero == null || model.Genero == "")
            {
                responseDto.Error = true;
                responseDto.Mensaje += "\n Debe indicar un Género";
            }
            return this.Json(responseDto);
        }
        public int GetDifferenceInYears(DateTime startDate, DateTime endDate)
        {
            int years = endDate.Year - startDate.Year;

            if (startDate.Month == endDate.Month &&
                endDate.Day < startDate.Day
                || endDate.Month < startDate.Month)
            {
                years--;
            }

            return years;
        }
    }
}