﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Web.UI;
namespace Evaluation.Controllers
{
    public static class Extensions
    {
        public static SelectList CreateSelectList<TSource>(this IEnumerable<TSource> items, string dataValueField, string dataTextField, bool valDefault = false, string defaultText = null)
        {
            SelectList slReturn = new SelectList(new object[] { });

            if (items != null)
            {
                if (valDefault)
                {
                    List<TSource> lstItems = items.ToList();
                    IList<SelectListItem> icItems = (from item in lstItems select new SelectListItem() { Text = Eval(item, dataTextField), Value = Eval(item, dataValueField) }).ToList();
                    icItems.Insert(0, new SelectListItem() { Text = string.IsNullOrWhiteSpace(defaultText) ? "-- Seleccione --" : defaultText, Value = string.Empty, Selected = true });

                    slReturn = new SelectList(icItems, "Value", "Text");
                }
                else
                {
                    slReturn = new SelectList(items, dataValueField, dataTextField);
                }
            }

            return slReturn;
        }
        private static string Eval(object container, string expression)
        {
            object obj = container;
            if (!string.IsNullOrEmpty(expression))
            {
                obj = DataBinder.Eval(container, expression);
            }
            return Convert.ToString(obj, CultureInfo.CurrentCulture);
        }
    }
}