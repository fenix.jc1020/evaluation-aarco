﻿namespace Evaluation.Dto
{
    public class CatalogoMarcaDto
    {
        public int iIdMarca { get; set; }
        public string sMarca { get; set; }
    }
    public class CatalogoSubmarcaDto
    {
        public int iIdSubMarca { get; set; }
        public int iIdMarcaSubramo { get; set; }
        public int iIdMostrar { get; set; }
        public string sSubMarca { get; set; }
    }
    public class CatalogoModeloDto
    {
        public int iIdModelo { get; set; }
        public string sModelo { get; set; }
    }
    public class CatalogoDescripcionDto
    {
        public int iIdDescripcionModelo { get; set; }
        public int iIdModeloSubmarca { get; set; }
        public int iIdMostrar { get; set; }
        public string sDescripcion { get; set; }
    }
}