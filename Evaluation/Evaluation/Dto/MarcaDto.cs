﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Evaluation.Dto
{
    public class MarcaDto
    {
        public List<CatalogoMarcaDto> catalogo { get; set; }
    }
}