﻿namespace Evaluation.Controllers
{
    public class ResponseDto
    {
        public string Mensaje { get; set; }
        public bool Error { get; set; }
    }
}