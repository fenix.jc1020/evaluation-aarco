﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Evaluation.Dto
{
    public class DescripcionDto
    {
        public List<CatalogoDescripcionDto> catalogo { get; set; }
    }
}