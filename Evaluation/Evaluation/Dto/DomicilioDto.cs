﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Evaluation.Dto
{
    public class DomicilioDto
    {
        public string cp { get; set; }
        public List<string> asentamiento { get; set; }
        public string tipo_asentamiento { get; set; }
        public string municipio { get; set; }
        public string estado { get; set; }
        public string ciudad { get; set; }
        public string pais { get; set; }
    }
}