﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Evaluation.Dto
{
    public class ModeloDto
    {
        public List<CatalogoModeloDto> catalogo { get; set; }
    }
}