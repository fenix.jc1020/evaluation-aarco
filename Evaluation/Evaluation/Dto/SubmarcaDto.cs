﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Evaluation.Dto
{
    public class SubmarcaDto
    {
        public List<CatalogoSubmarcaDto> catalogo { get; set; }
    }
}